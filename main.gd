@tool
extends Node2D

## HOWTO
##
## 1. Ensure sphere_scene.tscn is NOT open
##   To do this in v4.3.dev1.official [9d1cbab1c] you have to:
##    a. Close Godot
##    b. Open .godot/editor/editor_layout.cfg and edit the open_scenes array
##    c. Save that and when you restart Godot, that scene will be closed
## 2. Save and close Godot
## 3. Restart the project. Open main.tscn. Inspect the Node2D.
## 4. Press Inspect (bool) - prints resource_path fine
## 5. Open sphere_scene.tscn
## 6. Go back to main scene, press Inspect (bool) again - now resource_path is gone...
##
## Point 6 is the mystery.

var ps:PackedScene

@export var inspectPackedScene:bool:
	set(b):
		inspectPackedScene=false
		inspect()

func _ready() -> void:
	ps = load("res://sphere_scene.tscn")

func inspect():
	print()
	print("Inspect")
	print("  ps.resource_path=", ps.resource_path)
