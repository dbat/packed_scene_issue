#@tool
#extends Node2D
#
### HOWTO
###
### 1. Ensure sphere_scene.tscn is NOT open
###   To do this in v4.3.dev1.official [9d1cbab1c] you have to:
###    a. Close Godot
###    b. Open .godot/editor/editor_layout.cfg and edit the open_scenes array
###    c. Save that and when you restart Godot, that scene will be closed
### 2. Save and close Godot
### 3. Restart the project
### 4. Press save - shows resource_path fine
### 5. Open sphere_scene.tscn
### 6. Go back to main scene, press save again - resource_path is gone...
###
### Point 6 is the mystery.
#
#
#var DB:db = db.new()
#var ps:PackedScene# = preload("res://sphere_scene.tscn")
#
#@export var loadPackedScene:bool:
	#set(b):
		#loadPackedScene = false
		#rload()
#
#@export var inspectPackedScene:bool:
	#set(b):
		#inspectPackedScene=false
		#inspect()
#
#func _ready() -> void:
	#rload()
#
#func rload():
	#print()
	#print("Load")
	#ps = load("res://sphere_scene.tscn")
#
#func inspect():
	#print()
	#print("Inspect")
	#print("  ps.resource_path=", ps.resource_path)
